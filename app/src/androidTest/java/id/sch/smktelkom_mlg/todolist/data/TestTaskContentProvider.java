package id.sch.smktelkom_mlg.todolist.data;

/**
 * Created by SMK TELKOM on 3/15/2018.
 */

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class TestTaskContentProvider {
    private static final Uri TEST_TASKS = TaskContract.TaskEntry.CONTENT_URI;
    private static final Uri TEST_TASK_WITH_ID = TEST_TASKS.buildUpon().appendPath("1").build();
    private final Context mContext = InstrumentationRegistry.getTargetContext();

    @Before
    public void setUp() {
        /* Use TaskDbHelper to get access to a writable database */
        TaskDbHelper dbHelper = new TaskDbHelper(mContext);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(TaskContract.TaskEntry.TABLE_NAME, null, null);
    }

    @Test
    public void testProviderRegistry() {

        String packageName = mContext.getPackageName();
        String taskProviderClassName = TaskContentProvider.class.getName();
        ComponentName componentName = new ComponentName(packageName, taskProviderClassName);

        try {

            PackageManager pm = mContext.getPackageManager();

            ProviderInfo providerInfo = pm.getProviderInfo(componentName, 0);
            String actualAuthority = providerInfo.authority;
            String expectedAuthority = packageName;

            String incorrectAuthority =
                    "Error: TaskContentProvider registered with authority: " + actualAuthority +
                            " instead of expected authority: " + expectedAuthority;
            assertEquals(incorrectAuthority,
                    actualAuthority,
                    expectedAuthority);

        } catch (PackageManager.NameNotFoundException e) {
            String providerNotRegisteredAtAll =
                    "Error: TaskContentProvider not registered at " + mContext.getPackageName();

            fail(providerNotRegisteredAtAll);
        }
    }

    @Test
    public void testUriMatcher() {

        UriMatcher testMatcher = TaskContentProvider.buildUriMatcher();

        String tasksUriDoesNotMatch = "Error: The TASKS URI was matched incorrectly.";
        int actualTasksMatchCode = testMatcher.match(TEST_TASKS);
        int expectedTasksMatchCode = TaskContentProvider.TASKS;
        assertEquals(tasksUriDoesNotMatch,
                actualTasksMatchCode,
                expectedTasksMatchCode);

        String taskWithIdDoesNotMatch =
                "Error: The TASK_WITH_ID URI was matched incorrectly.";
        int actualTaskWithIdCode = testMatcher.match(TEST_TASK_WITH_ID);
        int expectedTaskWithIdCode = TaskContentProvider.TASK_WITH_ID;
        assertEquals(taskWithIdDoesNotMatch,
                actualTaskWithIdCode,
                expectedTaskWithIdCode);
    }

    @Test
    public void testInsert() {

        ContentValues testTaskValues = new ContentValues();
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, "Test description");
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_PRIORITY, 1);

        TestUtilities.TestContentObserver taskObserver = TestUtilities.getTestContentObserver();

        ContentResolver contentResolver = mContext.getContentResolver();

        contentResolver.registerContentObserver(
                TaskContract.TaskEntry.CONTENT_URI,
                true,
                taskObserver);


        Uri uri = contentResolver.insert(TaskContract.TaskEntry.CONTENT_URI, testTaskValues);


        Uri expectedUri = ContentUris.withAppendedId(TaskContract.TaskEntry.CONTENT_URI, 1);

        String insertProviderFailed = "Unable to insert item through Provider";
        assertEquals(insertProviderFailed, uri, expectedUri);

        taskObserver.waitForNotificationOrFail();

        contentResolver.unregisterContentObserver(taskObserver);
    }


    @Test
    public void testQuery() {

        TaskDbHelper dbHelper = new TaskDbHelper(mContext);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues testTaskValues = new ContentValues();
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, "Test description");
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_PRIORITY, 1);

        long taskRowId = database.insert(
                /* Table to insert values into */
                TaskContract.TaskEntry.TABLE_NAME,
                null,
                /* Values to insert into table */
                testTaskValues);

        String insertFailed = "Unable to insert directly into the database";
        assertTrue(insertFailed, taskRowId != -1);

        database.close();

        Cursor taskCursor = mContext.getContentResolver().query(
                TaskContract.TaskEntry.CONTENT_URI,
                /* Columns; leaving this null returns every column in the table */
                null,
                /* Optional specification for columns in the "where" clause above */
                null,
                /* Values for "where" clause */
                null,
                /* Sort order to return in Cursor */
                null);


        String queryFailed = "Query failed to return a valid Cursor";
        assertTrue(queryFailed, taskCursor != null);

        /* We are done with the cursor, close it now. */
        taskCursor.close();
    }

    @Test
    public void testDelete() {
        /* Access writable database */
        TaskDbHelper helper = new TaskDbHelper(InstrumentationRegistry.getTargetContext());
        SQLiteDatabase database = helper.getWritableDatabase();

        ContentValues testTaskValues = new ContentValues();
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, "Test description");
        testTaskValues.put(TaskContract.TaskEntry.COLUMN_PRIORITY, 1);

        long taskRowId = database.insert(
                /* Table to insert values into */
                TaskContract.TaskEntry.TABLE_NAME,
                null,
                /* Values to insert into table */
                testTaskValues);

        database.close();

        String insertFailed = "Unable to insert into the database";
        assertTrue(insertFailed, taskRowId != -1);


        TestUtilities.TestContentObserver taskObserver = TestUtilities.getTestContentObserver();

        ContentResolver contentResolver = mContext.getContentResolver();

        contentResolver.registerContentObserver(
                TaskContract.TaskEntry.CONTENT_URI,
                true,
                taskObserver);


        Uri uriToDelete = TaskContract.TaskEntry.CONTENT_URI.buildUpon().appendPath("1").build();
        int tasksDeleted = contentResolver.delete(uriToDelete, null, null);

        String deleteFailed = "Unable to delete item in the database";
        assertTrue(deleteFailed, tasksDeleted != 0);


        taskObserver.waitForNotificationOrFail();

        contentResolver.unregisterContentObserver(taskObserver);
    }

}
